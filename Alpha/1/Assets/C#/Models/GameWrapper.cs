﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.C_.Models
{
    public class GameWrapper
    {

        public const string BUILD_VERSION = "0.0.0"; // full.beta.alpha

        public GameWrapper()
        {
            HP = 3;

            LevelsPassed = 0;

            tictoc = new TicToc();
        }

        public int HP
        {
            get;

            set;
        }

        public int LevelsPassed
        {
            get;

            set;
        }

        public TicToc tictoc
        {
            get;

            set;
        }
    }
}
