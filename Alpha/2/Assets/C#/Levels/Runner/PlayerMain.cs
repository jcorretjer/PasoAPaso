﻿using Assets.C_.Models;
using Assets.C_.Models.Levels.Runner;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMain : MonoBehaviour
{
    private static Runner runner;

    void Start ()
    {
        runner = new Runner()
        {
            Body2D = GetComponent<Rigidbody2D>()
        };
	}

	void Update ()
    {
        runner.Body2D.transform.position = new Vector3((runner.Body2D.transform.position.x + 0.05f), runner.Body2D.transform.position.y);
	}

    public static void AddForceToPlayer(Vector2 forceVector)
    {
        runner.Body2D.AddForce(forceVector);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag.Equals(GameWrapper.Tags.COLLETIBLE.ToString()))
            collision.gameObject.SetActive(!collision.gameObject.activeSelf);

        //GameObject obj = GameObject.FindGameObjectWithTag(GameWrapper.Tags.COLLETIBLE.ToString());

        //obj.SetActive(false);
    }
}
