﻿using Assets.C_.Models.Levels.Runner;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickHandler : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        PlayerMain.AddForceToPlayer(new Vector2(0, Runner.Y_FORCE));
    }
}
