﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.C_.Models.Levels.Runner
{
    public class Runner: MonoBehaviour
    {
        public const float LOWEST_X_FORCE = 151f,
            Y_FORCE = 1000f;

        public Runner()
        {
            Body2D = new Rigidbody2D();
        }

        /// <summary>
        /// Components rigidbody2d
        /// </summary>
        public Rigidbody2D Body2D
        {
            get;

            set;
        }
    }
}
