﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.C_.Models
{
    public class TicToc
    {
        /// <summary>
        /// 
        /// </summary>
        public TicToc()
        {
            CurrentDuration = 10;

            DurationSubtractionFactor = 0;
        }

        public int CurrentDuration
        {
            get;

            set;
        }

        public int DurationSubtractionFactor
        {
            get;

            set;
        }
    }
}
